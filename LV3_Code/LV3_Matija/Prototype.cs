﻿using System;
using System.Collections.Generic;
using System.Text;

namespace LV3_Matija
{
    interface Prototype
    {
        Prototype Clone();
    }
}