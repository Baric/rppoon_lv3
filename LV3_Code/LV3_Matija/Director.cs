﻿using System;
using System.Collections.Generic;
using System.Text;

namespace LV3_Matija
{
    class Director
    {
        IBuilder builder = new NotificationBuilder();

        public ConsoleNotification BuildNotification_ERROR(String author)
        {
            return builder.SetAuthor(author).SetTitle("Error Title").SetText("Error").SetColor(ConsoleColor.DarkMagenta)
                .SetLevel(Category.ERROR).Build();
        }

        public ConsoleNotification BuildNotification_ALERT(String author)
        {
            return builder.SetAuthor(author).SetTitle("Alert").SetText("Alert").SetColor(ConsoleColor.White)
                .SetLevel(Category.ALERT).Build();
        }

        public ConsoleNotification BuildNotification_INFO(String author)
        {
            return builder.SetAuthor(author).SetTitle("Information").SetText("AppInformation").SetColor(ConsoleColor.DarkYellow)
               .SetLevel(Category.INFO).Build();
        }

    }
}