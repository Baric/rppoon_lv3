﻿using System;

namespace LV3_Matija
{
    class Program
    {
        static void Main(string[] args)
        {
            //Zad1
            Dataset data1 = new Dataset("D:\\University stuff\\RPPOON\\LV\\LV3\\LV3_Code\\LV3_Matija\\CSVFile.txt");
            Console.WriteLine(data1.PrintListElements());

            //Zad2
            MatrixGenerator generator = MatrixGenerator.GetInstance();
            double[][] matrix = generator.GenerateMatrix(5, 5);
            for (int i = 0; i < 5; i++)
            {
                for (int j = 0; j < 5; j++)
                {
                    Console.WriteLine(matrix[i][j] + " ");
                }
                Console.WriteLine("\n");
            }



            //Zad3
            Logger loger = Logger.GetInstance();
            string text = "Primjer se zapisuje u apendanu datoteku";
            loger.FilePath = "D:\\University stuff\\RPPOON\\LV\\LV3\\LV3_Code\\LV3_Matija\\Zad3.txt";
            loger.Logging(text);

            //Zad4
            Category category = Category.INFO;
            ConsoleColor color = ConsoleColor.Cyan;
            ConsoleNotification notification = new ConsoleNotification("Matija Barić", "Title", "Example text",
                new DateTime(2020, 4, 23, 9, 24, 55), category, color);
            NotificationMAnager manager = new NotificationMAnager();
            manager.Display(notification);

            //Zad5
            IBuilder builder = new NotificationBuilder();
            NotificationMAnager manager2 = new NotificationMAnager();
            manager2.Display(builder.Build());

            //Zad6
            NotificationMAnager manager3 = new NotificationMAnager();
            Director director = new Director();
            manager3.Display(director.BuildNotification_ALERT("James"));
            manager3.Display(director.BuildNotification_ERROR("Wilson"));
            manager3.Display(director.BuildNotification_INFO("Woodward"));


        }
    }
}