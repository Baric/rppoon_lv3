﻿using System;
using System.Collections.Generic;
using System.Text;

namespace LV3_Matija
{
    //Zad3
    class Logger
    {
        /*
        Sadržaj se nastavlja zapisivati u datoteku koju smo postavili na određeno mjesto
        ako koristimo njezinu istu instancu i ne mjenjamo setter
        */

        private static Logger instance;
        private string filePath;

        private Logger()
        {
            this.filePath = "D:\\University stuff\\RPPOON\\LV\\LV3\\LV3_Code\\LV3_Matija\\CSVprimjer1.txt";
        }

        public static Logger GetInstance()
        {
            if (instance == null)
            {
                instance = new Logger();
            }
            return instance;
        }

        public void Logging(string text)
        {
            using (System.IO.StreamWriter writer = new System.IO.StreamWriter(this.filePath, true))
            {
                writer.WriteLine(text.ToString());
            }

        }

        public string FilePath
        {
            set { this.filePath = value; }
        }
    }
}