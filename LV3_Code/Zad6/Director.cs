﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Zad6
{
    class Director
    {
        IBuilder builder = new NotificationBuilder();

        public ConsoleNotification BuildNotification_ERROR(String author)
        {
            return builder.SetAuthor(author).SetTitle("Error").SetText("Error").SetColor(ConsoleColor.DarkMagenta)
                .SetLevel(Category.ERROR).Build();
        }

        public ConsoleNotification BuildNotification_ALERT(String author)
        {
            return builder.SetAuthor(author).SetTitle("Error").SetText("Alert").SetColor(ConsoleColor.White)
                .SetLevel(Category.ALERT).Build();
        }

        public ConsoleNotification BuildNotification_INFO(String author)
        {
            return builder.SetAuthor(author).SetTitle("Info").SetText("Info").SetColor(ConsoleColor.DarkYellow)
               .SetLevel(Category.INFO).Build();
        }

    }
}