﻿using System;

namespace Zad6
{
    class Program
    {
        static void Main(string[] args)
        {

            NotificationMAnager manager3 = new NotificationMAnager();
            Director director = new Director();
            manager3.Display(director.BuildNotification_ALERT("James"));
            manager3.Display(director.BuildNotification_ERROR("Wilson"));
            manager3.Display(director.BuildNotification_INFO("Woodward"));
        }
    }
}