﻿using System;

namespace Zad4
{
    class Program
    {
        static void Main(string[] args)
        {
            Category category = Category.INFO;
            ConsoleColor color = ConsoleColor.Cyan;
            ConsoleNotification notification = new ConsoleNotification("Matija Barić", "Title", "Important text",
                new DateTime(2020, 4, 23, 9, 24, 55), category, color);
            NotificationMAnager manager = new NotificationMAnager();
            manager.Display(notification);
        }
    }
}