﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Zad1
{
    interface Prototype
    {
        Prototype Clone();
    }
}