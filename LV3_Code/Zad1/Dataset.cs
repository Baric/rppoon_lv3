﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Zad1
{
    class Dataset : Prototype
    {
        private List<List<string>> data; //list of lists of strings

        public Dataset()
        {
            this.data = new List<List<string>>();
        }

        public Dataset(string filePath) : this()
        {
            this.LoadDataFromCSV(filePath);
        }

        public void LoadDataFromCSV(string filePath)
        {
            using (System.IO.StreamReader reader = new System.IO.StreamReader(filePath))
            {
                string line;
                while ((line = reader.ReadLine()) != null)
                {
                    List<string> row = new List<string>();
                    string[] items = line.Split(',');
                    foreach (string item in items)
                    {
                        row.Add(item);
                    }
                    this.data.Add(row);
                }
            }
        }

        public IList<List<string>> GetData()
        {
            return new System.Collections.ObjectModel.ReadOnlyCollection<List<string>>(data);
        }

        public void ClearData()
        {
            this.data.Clear();
        }

        //Kod stringova nije potrebno duboko kopiranje
        public Prototype Clone()
        {
            //return (Prototype)this.MemberwiseClone();       //Plitko kopiranje
            Dataset clone = (Dataset)this.MemberwiseClone();
            clone.data = new List<List<string>>(this.data.Count);
            foreach (List<string> dataset in this.data)
            {
                clone.data.Add((List<string>)Clone());
            }
            return clone;
        }

        public string PrintListElements()
        {
            StringBuilder builder = new StringBuilder();
            foreach (List<string> data1 in this.data)
            {
                foreach (string data2 in data1)
                {
                    builder.Append(data2);
                }
                builder.Append("\n ");
            }
            return builder.ToString();
        }
    }
}