﻿using System;

namespace Zad2
{
    class Program
    {
        static void Main(string[] args)
        {

            MatrixGenerator generator = MatrixGenerator.GetInstance();
            double[][] matrix = generator.GenerateMatrix(5, 5);
            StringBuilder builder = new StringBuilder();
            for (int i = 0; i < 5; i++)
            {
                builder.Clear();
                for (int j = 0; j < 5; j++)
                {
                    builder.Append(matrix[i][j] + " ");
                }
                Console.WriteLine(builder.ToString());
                Console.WriteLine("\n");
            }
        }
    }
}