﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Zad7
{
    interface Prototype
    {
        Prototype Clone();
    }
}