﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Zad3
{
    class Logger
    {
        /*
        Sadržaj se nastavlja zapisivati u istu datoteku ako ju stavimo na jedno mjesto, ne mijenjamo setter, te radimo sa njezinom instancom 
        */
        private static Logger instance;
        private string filePath;

        private Logger()
        {
            this.filePath = "D:\\University stuff\\RPPOON\\LV\\LV3\\LV3_Code\\Zad3\\CSVFile.txt";
        }

        public static Logger GetInstance()
        {
            if (instance == null)
            {
                instance = new Logger();
            }
            return instance;
        }

        public void Logging(string text)
        {
            using (System.IO.StreamWriter writer = new System.IO.StreamWriter(this.filePath, true))
            {
                writer.WriteLine(text.ToString());

            }

        }

        public string FilePath
        {
            set { this.filePath = value; }
        }
    }
}